# grails-adminlte
Grails AdminLTE UI Plugin

## Disclaimer
I don't own Admin LTE nor its libraries and its plugins. All Admin LTE, its libraries and its plugins
are copyrighted and licenced by their respective owners.

## Introduction
This plugin will add Admin LTE, its required libraries, and supported plugins into your project.
It is based on Admin LTE v2.2.0 by [Almaseed Studio](https://almsaeedstudio.com).
You can see the demo of this project [here](https://almsaeedstudio.com/themes/AdminLTE/index2.html).
You can also see the documentation for Admin LTE usage [here](https://almsaeedstudio.com/themes/AdminLTE/documentation/index.html).

PS: This is my first open-source repo. If I made a mistake please correct me. :blush:

## Version Info
This is made with Grails v2.4.4 and it is currently supported only grails v2.x.x. I haven't tested on Grails v3.x.x yet.

## Installation
~~It is better to install directly from grails plugin repo by adding this line into your `BuildConfig.groovy`:~~
I haven't put this on the grails plugin repo
```groovy
plugins {
  ...
  runtime 'com.iqbaldjulfri:grails-ui-adminlte:1.0'
  ...
}
```

If you want to run locally, go to this project folder from your terminal and run `grails maven-install`
to register this project into your local maven repo. Then add the above lines into your project `BuildConfig.groovy`

## Usage Guide
I have created some taglibs to add required `<link>` and `<script>` tags into your GSP files.

##### `<adminlte:all />`
This tag simply put **ALL** `<link>` and `<script>` tags.

##### `<adminlte:allTags [css="true|false, default: true"] [js="true|false, default: true"] />`
This will put **ALL** `<link>` and `<script>` tags.
> Set `css` to `false` to deny `<link>` tag output.
>
> Set `js` to `false` to deny `<script>` tag output.

##### `<adminlte:coreTags [css="true|false, default: true"] [js="true|false, default: true"] />`
This will put **ONLY REQUIRED** `<link>` and `<script>` tags.
> Set `css` to `false` to deny `<link>` tag output.
>
> Set `js` to `false` to deny `<script>` tag output.

##### `<adminlte:plugin name="[plugin name]" [css="true|false, default: true"] [js="true|false, default: true"] />`
This will put a plugin `<link>` and `<script>` tags based on the specified `name` attribute.
> Set `css` to `false` to deny `<link>` tag output.
>
> Set `js` to `false` to deny `<script>` tag output.
>
> `name` is case-incensitive, and any dash (`-`) will be stripped automatically.

If no plugin were found, it will not produce any output.
List of supported plugins (plugin marked with `*` is required, and in that order):
```
plugin name             alias                   plugin name         alias
-----------------------------------------------------------------------------
jquery *                jquery                  fastclick
jqueryUI *              jqueryui                flot
bootstrap *             bootstrap               fullcalendar
admin-lte *             adminlte                iCheck              icheck
font-awesome *          fontawesome             inputmask
bootstrap-slider        bootstrapslider         ionslider
bootstrap-wysihtml5     bootstrapwysihtml5      jvectormap
chartjs                                         knob
ckeditor                                        morris
colorpicker                                     pace
datatables                                      select2
datepicker                                      sparkline
daterangepicker                                 timepicker
```

##### `<adminlte:sliderPlugins [css="true|false, default: true"] [js="true|false, default: true"] />`
This will put slider plugin related `<link>` and `<script>` tags.
> Set `css` to `false` to deny `<link>` tag output.
>
> Set `js` to `false` to deny `<script>` tag output.

##### `<adminlte:chartPlugins [css="true|false, default: true"] [js="true|false, default: true"] />`
This will put chart plugin related `<link>` and `<script>` tags.
> Set `css` to `false` to deny `<link>` tag output.
>
> Set `js` to `false` to deny `<script>` tag output.

## Change Log
- 1.0.0 - Inital Release, updated readme
- 0.9.0 - Change some tag names, finish the readme

