package com.iqbaldjulfri.grailsplugins.taglibs

class AdminLTETagLib {
  static namespace = "adminlte"

  /**
   * Generates {@code <link>} and {@code <script>} tags
   *
   * @attr css set to {@code false} to not generate CSS tags ({@code <link>}). Default: {@code true}
   * @attr js set to {@code false} to not generate JS tags ({@code <script>}). Default: {@code true}
   */
  def allTags = { attrs ->
    def css = attrs.css ?: true
    def js = attrs.js ?: true

    if (css) out << """
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap/css", file: "bootstrap.min.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/font-awesome/css", file: "font-awesome.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap-slider", file: "slider.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap-wysihtml5", file: "bootstrap3-wysihtml5.min.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/colorpicker", file: "bootstrap-colorpicker.min.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/datatables", file: "dataTables.bootstrap.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/datepicker", file: "datepicker3.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/daterangepicker", file: "daterangepicker-bs3.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/fullcalendar", file: "fullcalendar.min.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/iCheck", file: "all.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/ionslider", file: "ion.rangeSlider.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/jvectormap", file: "jquery-jvectormap-1.2.2.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/morris", file: "morris.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/select2", file: "select2.min.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/timepicker", file: "bootstrap-timepicker.min.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/admin-lte/css", file: "AdminLTE.min.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/admin-lte/css/skins", file: "_all-skins.min.css")}"/>
"""
    if (js) out << """
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/jQuery", file: "jQuery-2.1.4.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/jQueryUI", file: "jquery-ui.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap/js", file: "bootstrap.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/admin-lte/js", file: "app.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap-slider", file: "bootstrap-slider.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap-wysihtml5", file: "bootstrap3-wysihtml5.all.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/chartjs", file: "Chart.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/ckeditor", file: "ckeditor.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/colorpicker", file: "bootstrap-colorpicker.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/datatables", file: "jquery.dataTables.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/datatables", file: "dataTables.bootstrap.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/datepicker", file: "bootstrap-datepicker.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/daterangepicker", file: "daterangepicker.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/daterangepicker", file: "moment.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/fastclick", file: "fastclick.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "excanvas.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.colorhelpers.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.canvas.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.categories.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.crosshair.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.errorbars.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.fillbetween.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.image.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.navigate.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.pie.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.resize.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.selection.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.stack.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.symbol.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.threshold.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.time.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/fullcalendar", file: "fullcalendar.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/iCheck", file: "icheck.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.extensions.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.date.extensions.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.numeric.extensions.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.phone.extensions.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.regex.extensions.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/ionslider", file: "ion.rangeSlider.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/jvectormap", file: "jquery-jvectormap-1.2.2.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/knob", file: "jquery.knob.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/morris", file: "morris.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/pace", file: "pace.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/select2", file: "select2.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/slimScroll", file: "jquery.slimscroll.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/sparkline", file: "jquery.sparkline.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/timepicker", file: "bootstrap-timepicker.min.js")}"></script>
"""
  }

  def all = {
    out << allTags()
  }

  /**
   * Generates core plugin {@code <link>} and {@code <script>} tags
   *
   * @attr css set to {@code false} to not generate CSS tags ({@code <link>}). Default: {@code true}
   * @attr js set to {@code false} to not generate JS tags ({@code <script>}). Default: {@code true}
   */
  def coreTags = { attrs ->
    def css = attrs.css ?: true
    def js = attrs.js ?: true

    if (css) {
      out << """
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap/css", file: "bootstrap.min.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/admin-lte/css", file: "AdminLTE.min.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/css/skins", file: '_all-skins.min.css', plugin: 'ui-adminlte')}"/>
"""
    }

    if (js) {
      out << """
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/jQuery", file: "jQuery-2.1.4.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/jQueryUI", file: "jquery-ui.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap/js", file: "bootstrap.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/admin-lte/js", file: "app.min.js")}"></script>
"""
    }
  }

  /**
   * Generates slider plugin {@code <link>} and {@code <script>} tags
   *
   * @attr css set to {@code false} to not generate CSS tags ({@code <link>}). Default: {@code true}
   * @attr js set to {@code false} to not generate JS tags ({@code <script>}). Default: {@code true}
   */
  def sliderPlugins = { attrs ->
    def css = attrs.css ?: true
    def js = attrs.js ?: true

    if (css) {
      out << """
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap-slider", file: "slider.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/ionslider", file: "ion.rangeSlider.css")}"/>
"""
    }

    if (js) {
      out << """
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap-slider", file: "bootstrap-slider.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/ionslider", file: "ion.rangeSlider.min.js")}"></script>
"""
    }
  }

  /**
   * Generates chart plugin {@code <link>} and {@code <script>} tags
   *
   * @attr css set to {@code false} to not generate CSS tags ({@code <link>}). Default: {@code true}
   * @attr js set to {@code false} to not generate JS tags ({@code <script>}). Default: {@code true}
   */
  def chartPlugins = { attrs ->
    def css = attrs.css ?: true
    def js = attrs.js ?: true

    if (css) out << """
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/jvectormap", file: "jquery-jvectormap-1.2.2.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/morris", file: "morris.css")}"/>
"""
    if (js) out << """
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/chartjs", file: "Chart.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "excanvas.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.colorhelpers.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.canvas.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.categories.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.crosshair.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.errorbars.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.fillbetween.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.image.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.navigate.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.pie.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.resize.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.selection.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.stack.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.symbol.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.threshold.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.time.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/jvectormap", file: "jquery-jvectormap-1.2.2.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/knob", file: "jquery.knob.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/morris", file: "morris.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/pace", file: "pace.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/sparkline", file: "jquery.sparkline.min.js")}"></script>
"""
  }

  /**
   * Generates single plugin {@code <link>} and {@code <script>} tag
   *
   * @attr name REQUIRED plugin name. If plugin were not found, it will not produce any output
   * @attr css set to {@code false} to not generate CSS tags ({@code <link>}). Default: {@code true}
   * @attr js set to {@code false} to not generate JS tags ({@code <script>}). Default: {@code true}
   */
  def plugin = { attrs ->
    def css = attrs.css ?: true
    def js = attrs.js ?: true
    def name = (attrs.name ?: "").toLowerCase().trim().split('-').join()

    def plugins = [
        jquery: [
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/jQuery", file: "jQuery-2.1.4.min.js")}"></script>""",
        ],
        jqueryui: [
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/jQueryUI", file: "jquery-ui.min.js")}"></script>"""
        ],
        bootstrap: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap/css", file: "bootstrap.min.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap/js", file: "bootstrap.min.js")}"></script>"""
        ],
        adminlte: [
            css: [
                """
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/admin-lte/css", file: "AdminLTE.min.css")}"/>
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/css/skins", file: '_all-skins.min.css', plugin: 'ui-adminlte')}"/>
"""
            ],
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/admin-lte/js", file: "app.min.js")}"></script>"""
        ],
        fontawesome: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/font-awesome/css", file: "font-awesome.css")}"/>"""
        ],
        bootstrapslider: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap-slider", file: "slider.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap-slider", file: "bootstrap-slider.js")}"></script>"""
        ],
        bootsrapwysihtml5: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap-wysihtml5", file: "bootstrap3-wysihtml5.min.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/bootstrap-wysihtml5", file: "bootstrap3-wysihtml5.all.min.js")}"></script>"""
        ],
        chartjs: [
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/chartjs", file: "Chart.min.js")}"></script>"""
        ],
        ckeditor: [
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/ckeditor", file: "ckeditor.js")}"></script>"""
        ],
        colopicker: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/colorpicker", file: "bootstrap-colorpicker.min.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/colorpicker", file: "bootstrap-colorpicker.min.js")}"></script>"""
        ],
        datatables: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/datatables", file: "dataTables.bootstrap.css")}"/>""",
            js: """
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/datatables", file: "jquery.dataTables.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/datatables", file: "dataTables.bootstrap.min.js")}"></script>
"""
        ],
        datepicker: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/datepicker", file: "datepicker3.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/datepicker", file: "bootstrap-datepicker.js")}"></script>"""
        ],
        daterangepicker: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/daterangepicker", file: "daterangepicker-bs3.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/daterangepicker", file: "daterangepicker.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/daterangepicker", file: "moment.min.js")}"></script>"""
        ],
        fastclick: [
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/fastclick", file: "fastclick.min.js")}"></script>"""
        ],
        flot: [
            js: """
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "excanvas.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.colorhelpers.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.canvas.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.categories.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.crosshair.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.errorbars.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.fillbetween.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.image.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.navigate.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.pie.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.resize.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.selection.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.stack.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.symbol.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.threshold.min.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/flot", file: "jquery.flot.time.min.js")}"></script>
"""
        ],
        fullcalendar: [
            css: """
<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/fullcalendar", file: "fullcalendar.min.css")}"/>
""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/fullcalendar", file: "fullcalendar.min.js")}"></script>"""
        ],
        icheck: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/iCheck", file: "all.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/iCheck", file: "icheck.min.js")}"></script>"""
        ],
        inputmask: [
            js: """
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.extensions.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.date.extensions.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.numeric.extensions.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.phone.extensions.js")}"></script>
<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/input-mask", file: "jquery.inputmask.regex.extensions.js")}"></script>
"""
        ],
        ionslider: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/ionslider", file: "ion.rangeSlider.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/ionslider", file: "ion.rangeSlider.min.js")}"></script>"""
        ],
        jvectormap: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/jvectormap", file: "jquery-jvectormap-1.2.2.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/jvectormap", file: "jquery-jvectormap-1.2.2.min.js")}"></script>"""
        ],
        knob: [
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/knob", file: "jquery.knob.js")}"></script>"""
        ],
        morris: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/morris", file: "morris.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/morris", file: "morris.min.js")}"></script>"""
        ],
        pace: [
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/pace", file: "pace.js")}"></script>"""
        ],
        select2: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/select2", file: "select2.min.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/select2", file: "select2.full.min.js")}"></script>"""
        ],
        slimscroll: [
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/slimScroll", file: "jquery.slimscroll.min.js")}"></script>"""
        ],
        sparkline: [
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/sparkline", file: "jquery.sparkline.min.js")}"></script>"""
        ],
        timepicker: [
            css: """<link rel="stylesheet" href="${resource(dir: "${pluginContextPath}/admin-lte/timepicker", file: "bootstrap-timepicker.min.css")}"/>""",
            js: """<script type="text/javascript" src="${resource(dir: "${pluginContextPath}/admin-lte/timepicker", file: "bootstrap-timepicker.min.js")}"></script>"""
        ]
    ]

    out << plugins[name].js << plugins[name].js
  }
}
