package com.iqbaldjulfri.grailsplugins.taglibs

/**
 * Created by iqbal on 8/11/2015.
 */
class CustomFormTagLib {
  static namespace = "adminlte"

  /**
   * A simple date picker that renders a date as selects.<br/>
   * e.g. &lt;g:datePicker name="myDate" value="${new Date()}" /&gt;
   *
   * @emptyTag
   *
   * @attr name REQUIRED The name of the date picker field set
   * @attr value The current value of the date picker; defaults to either the value specified by the default attribute or now if no default is set
   * @attr default A Date or parsable date string that will be used if there is no value
   * @attr id the DOM element id
   * @attr disabled Makes the resulting inputs and selects to be disabled. Is treated as a Groovy Truth.
   * @attr readonly Makes the resulting inputs and selects to be made read only. Is treated as a Groovy Truth.
   */
  def datepicker = { attrs ->
    def dateFormat = attrs.format ?: message(code: 'adminlte.plugin.datepicker.date-format', default: 'yyyy-MM-dd')
    def dataDateFormat = convertJavaDateFormatToDatepickerDateFormat(dateFormat)
    def putMask = attrs.mask == 'false' ? false : true
    println("putMask = $putMask")
    def disabled = attrs.disabled == 'true'
    def readonly = attrs.readonly == 'true'
    def date = attrs.val ?: attrs.'default' ?: null
    def val = null
    def name = attrs.name
    def id = attrs.id ?: name

    if (date instanceof Date) val = ((Date) date).format(dateFormat)
    else if (date instanceof String) val = date


    out << '<div class="input-group">'
    out << '<div class="input-group-addon"><i class="fa fa-calendar"></i></div>'
    out << '<input type="text" '
    out << """name="${name}" """
    out << """id="${id}" """
    out << """class="form-control init-datepicker ${putMask ? 'init-inputmask' : ''}" """
    out << """value="${val ?: ''}" """
    out << """data-date-format="${dataDateFormat}" """
    if (putMask && !dataDateFormat.contains('M') && !dataDateFormat.contains('D'))
      out << """data-inputmask="'alias': '${dataDateFormat}', 'placeholder': '${dataDateFormat}'" """
    else
      out << """placeholder="${dataDateFormat}" """
    if (disabled) out << 'disabled '
    if (readonly) out << 'readonly '
    out << '/>'
    out << '</div>'


  }

  /**
   * MMMM > MM
   * MMM > M
   * MM > mm
   * M > m
   *
   * dddd > DD
   * ddd > D
   * dd > dd
   * d > d
   */
  private convertJavaDateFormatToDatepickerDateFormat(String format) {
    final def MMMM = 'D@i~2#'
    final def MMM = 'o$P#7'
    return format.replaceAll('MMMM', MMMM).replaceAll('MMM', MMM)
        .replaceAll('MM', 'mm').replaceAll('M', 'm')
        .replaceAll(MMMM, 'MM').replaceAll(MMM, 'M')
        .replaceAll('dddd', 'DD').replaceAll('ddd', 'D')
  }
}
